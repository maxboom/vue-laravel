
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));

import VueResource from 'vue-resource';

import DataGrid from './components/DataGrid/DataGrid.vue';
import DataTable from './components/Table/Table.vue';
import DataEdit from './components/DataEdit/DataEdit.vue';
import Pagination from './components/Pagination/Pagination.vue';

Vue.component('data-grid', DataGrid);
Vue.component('grid-table', DataTable);
Vue.component('data-edit', DataEdit);
Vue.component('pagination', Pagination);

export const apiUrl = '/api/users';


Vue.use(VueResource);

Vue.http.interceptors.push(function(request, next) {
    request.headers['X-CSRF-TOKEN'] = Laravel.csrfToken;
    next();
})

const app = new Vue({
    el: '#app'
});
