import Config from '../../config.js';

export default {
    props: {
        show: {
            type: Boolean,
            default: false
        },
        data: {
            type: Object,
            required: true
        },
        editColumns: {
            type: Object,
            required: true
        },
        createMode: {
            type: Boolean,
            default: false
        }
    },
    data: function() {
        return {
            formData: {},
            formErrors: {}
        }
    },
    created: function() {
        this.reset();
    },
    methods: {
        reset: function() {
            this.formData = Object.assign({}, this.formData, this.data);
            this.formErrors = {};//Object.assign({}, this.formErrors, {});
        },
        hide: function() {
            this.$emit('onClose');
        },
        save: function() {
            if (this.createMode) {
                return this.create();
            }
            this.$http.put(Config.apiUrl + '/' + this.formData['id'], this.formData)
                .then((response) => {
                    console.log(response);
                    if (this.setFieldsErrors(response)) {
                        return false;
                    }

                    this.$emit('onChange', this.formData);
                    this.hide();
                }, (errorResponse) => {
                    console.error(this.errorResponse);
                });
        },
        create: function() {
            this.$http.post(Config.apiUrl, this.formData)
                .then((response) => {
                    console.log(response);
                    if (this.setFieldsErrors(response)) {
                        return false;
                    }

                    this.$emit('onCreate', this.formData);
                    this.hide();
                }, (errorResponse) => {
                    console.error(this.errorResponse);
                })
        },
        setFieldsErrors: function (response) {
            if (!response.data.hasOwnProperty('errors')) {
                return false;
            }

            this.formErrors = response.data.errors;

            // this.formErrors = Object.assign(
            //     {},
            //     this.formErrors,
            //     response.data.errors
            // );

            return true;
        },
        getErorr: function(field) {
            return (this.formErrors && this.formErrors.hasOwnProperty(field)) ?
                this.formErrors[field][0] :
                false;
        }
    },
    computed: {
        dataChenged: function() {
            return JSON.stringify(this.data) ==
                   JSON.stringify(this.formData);
        },
        isShow: function() {
            this.reset();
            return this.show;
        }
    }
}