export default {
    props: {
        countPages: {
            type: Number,
            required: true
        },
        currentPage: {
            type: Number,
            required: true
        }
    },
    methods: {
        setPage: function(page) {
            this.$emit('onPageChenged', page);
        },
        setNext: function() {
            this.setPage(this.currentPage + 1);
        },
        setPrevious: function() {
            this.setPage(this.currentPage - 1);
        }
    }
}