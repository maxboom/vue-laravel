import DataEdit from '../DataEdit/DataEdit.vue';


export default {
    components: {
        DataEdit
    },
    props: {
        data: Array,
        columns: {
            type: Object,
            required: true
        },
        createColumns: {
            type: Object
        },
        placeholders: {
            type: Object
        },
        dataFilters: {
            type: Object,
            default: []
        },
        allowSelection: {
            type: Boolean,
            default: true
        },
        allowEdit: {
            type: Boolean,
            default: false
        }
    },
    data: function () {
        return {
            selectedRows: [],
            editingRow: null,
            dataPlaceholders: (this.placeholders) ? this.placeholders : this.columns,
            dataCreateColumns: (this.createColumns) ? this.createColumns : this.columns
        }
    },
    methods: {
        deleteSeleted: function() {
            this.$emit('onDelete', this.selectedRows);
            this.selectedRows = [];
            //console.log(this.selectedRows);
        },
        saveData: function(data) {
            this.closeModal();
            this.$emit('onSave', data);
        },
        createData: function (data) {
            this.closeModal();
            this.$emit('onCreate', data)
        },
        closeModal: function() {
            this.editingRow = null;
        },
        getEmpty: function() {
            return this.dataPlaceholders;
        },
        setEditingRow: function(modalName) {
            this.editingRow = modalName;
        }
    },
    computed: {
        groupByFilter: function () {
            return this.data.filter((row) => {
                for (var field in row) {
                    if (this.dataFilters.hasOwnProperty(field) &&
                        this.dataFilters[field] &&
                        row[field].search(this.dataFilters[field]) < 0
                        )
                        {
                            return false;
                        }
                }
                return true;
                
            });
        }
    }
}