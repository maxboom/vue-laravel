import DataTable from '../Table/Table.vue';
import Pagination from '../Pagination/Pagination.vue';
import Config from '../../config.js';

export default {
    components: {
        DataTable,
        Pagination
    },
    data: function () {
        return {
            response: {},
            error: ''
        };
    },
    created: function() {
        this.loadData(Config.apiUrl);
    },
    methods: {
        loadData: function(url) {
            this.$http.get(url).then((response) => {
                this.response = Object.assign(
                    {}, 
                    this.response, 
                    response.data
                );
            }, (errorResponse) => {
                console.error(this.errorResponse);
                this.error = errorResponse.body;
            });
        },
        loadByPage: function(page) {
            this.loadData(Config.apiUrl + '?page=' + page);
        },
        saveData: function(data) {
            this.loadByPage(this.response.current_page);
        },
        deleteRows: function (data) {
            for (var row in data) {
                this.$http.delete(Config.apiUrl + '/' + data[row])
                    .then((response) => {
                        this.loadByPage(this.response.current_page);
                    }, (errorResponse) => {
                        this.error = errorResponse.body;
                    });
            }
        }
    }
}